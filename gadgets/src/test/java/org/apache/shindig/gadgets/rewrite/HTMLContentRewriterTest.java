package org.apache.shindig.gadgets.rewrite;

import org.apache.commons.io.IOUtils;
import org.apache.shindig.gadgets.http.HttpRequest;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HTMLContentRewriterTest extends BaseRewriterTestCase {

  private HTMLContentRewriter rewriter;

  @Before
  public void setUp() throws Exception {
    super.setUp();
    ContentRewriterFeature overrideFeature =
        rewriterFeatureFactory.get(createSpecWithRewrite(".*", ".*exclude.*", "HTTP",
            HTMLContentRewriter.TAGS));
    ContentRewriterFeatureFactory factory = mockContentRewriterFeatureFactory(overrideFeature);
    rewriter = new HTMLContentRewriter(factory, DEFAULT_PROXY_BASE, DEFAULT_CONCAT_BASE);

  }

  @Test
  public void testScriptsBasic() throws Exception {
    String content = readFile("org/apache/shindig/gadgets/rewrite/rewritescriptbasic.html");
    Document doc = rewriteContent(rewriter, content).getDocument();

    XPathWrapper wrapper = new XPathWrapper(doc);

    // Head should contain 1 script tag
    assertEquals(wrapper.getValue("/html/head/script"), "headScript1");
    assertEquals(wrapper.getNodeList("/html/head/script").getLength(), 1);

    // Body should contain 8 script tags after rewrite
    assertEquals(wrapper.getNodeList("/html/body/script").getLength(), 8);

    assertEquals(wrapper.getValue("/html/body/script[1]"), "bodyScript1");

    // Second script should contain two concatenated urls
    assertEquals(wrapper.getValue("/html/body/script[2]/@src"),
        "http://www.test.com/dir/concat?" +
            "rewriteMime=text/javascript&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml" +
            "&fp=1150739864" +
            "&1=http%3A%2F%2Fwww.example.org%2F1.js" +
            "&2=http%3A%2F%2Fwww.example.org%2F2.js");

    assertEquals(wrapper.getValue("/html/body/script[3]"), "bodyScript2");

    // Fourth script should contain one concatenated url
    assertEquals(wrapper.getValue("/html/body/script[4]/@src"),
        "http://www.test.com/dir/concat?" +
            "rewriteMime=text/javascript" +
            "&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml" +
            "&fp=1150739864" +
            "&1=http%3A%2F%2Fwww.example.org%2F3.js");

    // Fifth script should contain a retained comment
    assertEquals(wrapper.getValue("/html/body/script[5]"),
        "<!-- retain-comment -->");

    // An excluded URL between contiguous tags prevents them being concatentated
    assertEquals(wrapper.getValue("/html/body/script[6]/@src"),
        "http://www.test.com/dir/concat?" +
            "rewriteMime=text/javascript&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml" +
            "&fp=1150739864" +
            "&1=http%3A%2F%2Fwww.example.org%2F4.js");

    // Excluded URL is untouched
    assertEquals(wrapper.getValue("/html/body/script[7]/@src"),
        "http://www.example.org/excluded/5.js");

    assertEquals(wrapper.getValue("/html/body/script[8]/@src"),
        "http://www.test.com/dir/concat?" +
            "rewriteMime=text/javascript&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml" +
            "&fp=1150739864" +
            "&1=http%3A%2F%2Fwww.example.org%2F6.js");

  }

  @Test
  public void testLinksBasic() throws Exception {
    String content = readFile("org/apache/shindig/gadgets/rewrite/rewritelinksbasic.html");
    Document doc = rewriteContent(rewriter, content).getDocument();

    XPathWrapper wrapper = new XPathWrapper(doc);

    // Image is rewritten to proxy, relative path is resolved
    assertEquals(wrapper.getValue("//img[1]/@src"),
        "http://www.test.com/dir/proxy?" +
            "url=http%3A%2F%2Fwww.example.org%2Fimg.gif" +
            "&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml&fp=1150739864");

    // Excluded image is untouched
    assertEquals(wrapper.getValue("//img[2]/@src"), "http://www.example.org/excluded/img.gif");

    // Embed target is rewritten to proxy
    assertEquals(wrapper.getValue("//embed[1]/@src"),
        "http://www.test.com/dir/proxy?" +
            "url=http%3A%2F%2Fwww.example.org%2Fsome.swf" +
            "&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml&fp=1150739864");

    // Excluded embed is untouched
    assertEquals(wrapper.getValue("//embed[2]/@src"), "http://www.example.org/excluded/some.swf");
  }

  @Test
  public void testStyleBasic() throws Exception {
    String content = readFile("org/apache/shindig/gadgets/rewrite/rewritestylebasic.html");
    Document doc = rewriteContent(rewriter, content).getDocument();

    XPathWrapper wrapper = new XPathWrapper(doc);

    // ALL style links and @import targets are rewritten to concat
    // Note that relative URLs are fully resolved
    assertEquals(wrapper.getValue("//link[1]/@href"),
        "http://www.test.com/dir/concat?" +
            "rewriteMime=text/css&gadget=http%3A%2F%2Fwww.example.org%2Fdir%2Fg.xml&fp=1150739864" +
            "&1=http%3A%2F%2Fwww.example.org%2Flinkedstyle1.css" +
            "&2=http%3A%2F%2Fwww.example.org%2Flinkedstyle3.css" +
            "&3=http%3A%2F%2Fwww.example.org%2Fimportedstyle1.css" +
            "&4=http%3A%2F%2Fwww.example.org%2Fimportedstyle3.css" +
            "&5=http%3A%2F%2Fwww.example.org%2Fimportedstyle4.css");

    // Untouched link target
    assertEquals(wrapper.getValue("//link[2]/@href"),
        "http://www.example.org/excluded/linkedstyle2.css");

    // Untouched @import taget converted to a link
    assertEquals(wrapper.getValue("//link[3]/@href"),
        "/excluded/importedstyle2.css");

    // Body should contain 1 style element
    assertEquals(wrapper.getNodeList("//style").getLength(), 1);

    // All @imports are stripped
    assertEquals(wrapper.getValue("//style[1]"),
        "div { color : black; }");
  }

  private String readFile(final String relativePath) throws IOException {
    final InputStream inputStream = getClass().getClassLoader().getResourceAsStream(relativePath);
    assertNotNull("No such file '" + relativePath + "'", inputStream);
    return IOUtils.toString(inputStream, UTF_8);
  }

  @Test
  public void testNoRewriteUnknownMimeType() {
    // Strict mock as we expect no calls
    MutableContent mc = mock(MutableContent.class);
    HttpRequest req = mock(HttpRequest.class);
    when(req.getRewriteMimeType()).thenReturn("unknown");

    assertNull(rewriter.rewrite(req, fakeResponse, mc));
  }
}
