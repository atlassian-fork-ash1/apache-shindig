package org.apache.shindig.gadgets.variables;

import org.apache.shindig.gadgets.UserPrefs;
import org.apache.shindig.gadgets.spec.GadgetSpec;
import org.apache.shindig.gadgets.spec.UserPref;

import static org.apache.commons.text.StringEscapeUtils.escapeHtml4;

/**
 * Substitutes user prefs into the spec.
 */
public class UserPrefSubstituter {

    public static void addSubstitutions(final Substitutions substituter, final GadgetSpec spec, final UserPrefs values) {
        for (UserPref pref : spec.getUserPrefs()) {
            String name = pref.getName();
            String value = values.getPref(name);
            if (value == null) {
                value = pref.getDefaultValue();
                if (value == null) {
                    value = "";
                }
            }
            substituter.addSubstitution(Substitutions.Type.USER_PREF, name, escapeHtml4(value));
        }
    }
}
