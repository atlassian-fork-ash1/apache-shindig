package org.apache.shindig.social.core.util;

import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.core.ClassLoaderReference;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.thoughtworks.xstream.mapper.DefaultMapper;
import com.thoughtworks.xstream.mapper.Mapper;
import org.apache.shindig.social.core.util.xstream.StackDriver;
import org.apache.shindig.social.core.util.xstream.ThreadSafeWriterStack;
import org.apache.shindig.social.core.util.xstream.WriterStack;
import org.apache.shindig.social.core.util.xstream.XStreamConfiguration;
import org.apache.shindig.social.core.util.xstream.XStreamConfiguration.ConverterConfig;
import org.apache.shindig.social.opensocial.service.BeanConverter;
import org.apache.shindig.social.opensocial.spi.DataCollection;
import org.apache.shindig.social.opensocial.spi.RestfulCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class BeanXStreamConverter implements BeanConverter {

  // Who knows what this might be used for; it's public API now.
  @SuppressWarnings("unused")
  public static final String XML_DECL = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

  private static final XStreamConfiguration.ConverterSet[] MAPPER_SCOPES = {
      XStreamConfiguration.ConverterSet.MAP,
      XStreamConfiguration.ConverterSet.COLLECTION,
      XStreamConfiguration.ConverterSet.DEFAULT
  };

  private static final Logger log = LoggerFactory.getLogger(BeanXStreamConverter.class);

  final WriterStack writerStack;
  final Map<XStreamConfiguration.ConverterSet, ConverterConfig> converterMap = Maps.newHashMap();

  @Inject
  public BeanXStreamConverter(XStreamConfiguration configuration) {
    final ReflectionProvider rp = new PureJavaReflectionProvider();
    final Mapper defaultMapper = new DefaultMapper(new ClassLoaderReference(getClass().getClassLoader()));
    /*
     * Putting this here means only one conversion per thread may be active at
     * any one time, but since the conversion process is atomic this will not
     * matter unless the class is extended.
     */
    writerStack = new ThreadSafeWriterStack();

    /*
     * create a driver that wires into a standard driver, and updates the stack
     * position.
     */
    final HierarchicalStreamDriver driver = new StackDriver(new XppDriver(), writerStack, configuration.getNameSpaces());
    /*
     * Create an interface class mapper that understands class hierarchy for
     * single items
     */
    for (XStreamConfiguration.ConverterSet converterSet : MAPPER_SCOPES) {
      final ConverterConfig converterConfig =
              configuration.getConverterConfig(converterSet, rp, defaultMapper, driver, writerStack);
      converterMap.put(converterSet, converterConfig);
    }
  }

  public String getContentType() {
    return "application/xml";
  }

  public String convertToString(Object pojo) {
    return convertToXml(pojo);
  }

  /**
   * convert an Object to XML, but make certain that only one of these is run on
   * a thread at any one time. This only matters if this class is extended.
   *
   * @param obj the object to convert
   * @return the XML
   */
  private String convertToXml(final Object obj) {

    writerStack.reset();
    if (obj instanceof Map) {
      Map<?, ?> m = (Map<?, ?>) obj;
      ConverterConfig cc = converterMap
          .get(XStreamConfiguration.ConverterSet.MAP);
      if (m.size() == 1) {
        Object s = m.values().iterator().next();
        cc.mapper.setBaseObject(s); // thread safe method
        String result = cc.xstream.toXML(s);
        log.debug("Result is " + result);
        return "<response>" + result + "</response>";
      }
    } else if (obj instanceof RestfulCollection) {
      ConverterConfig cc = converterMap
          .get(XStreamConfiguration.ConverterSet.COLLECTION);
      cc.mapper.setBaseObject(obj); // thread safe method
      String result = cc.xstream.toXML(obj);
      log.debug("Result is " + result);
      return result;
    } else if (obj instanceof DataCollection) {
      ConverterConfig cc = converterMap
          .get(XStreamConfiguration.ConverterSet.MAP);
      cc.mapper.setBaseObject(obj); // thread safe method
      String result = cc.xstream.toXML(obj);
      log.debug("Result is " + result);
      return result;
    }
    ConverterConfig cc = converterMap
        .get(XStreamConfiguration.ConverterSet.DEFAULT);

    cc.mapper.setBaseObject(obj); // thread safe method
    String result = cc.xstream.toXML(obj);
    log.debug("Result is " + result);
    return "<response>" + result + "</response>";
  }

  @SuppressWarnings("unchecked")
  public <T> T convertToObject(String xml, Class<T> className) {
    ConverterConfig cc = converterMap.get(XStreamConfiguration.ConverterSet.DEFAULT);
    return (T) cc.xstream.fromXML(xml);
  }
}
