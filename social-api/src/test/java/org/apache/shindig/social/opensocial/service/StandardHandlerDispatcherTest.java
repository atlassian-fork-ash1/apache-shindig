/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.shindig.social.opensocial.service;

import com.google.inject.Provider;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests StandardHandlerDispatcher.
 */
public class StandardHandlerDispatcherTest {

  private StandardHandlerDispatcher dispatcher;
  private Provider<PersonHandler> personHandlerProvider;

  @SuppressWarnings("unchecked")
  @Before
  public void setUp() throws Exception {
    personHandlerProvider = (Provider<PersonHandler>)  mock(Provider.class);
    Provider<AppDataHandler> appDataHandlerProvider = (Provider<AppDataHandler>) mock(Provider.class);
    Provider<ActivityHandler> activityHandlerProvider = (Provider<ActivityHandler>) mock(Provider.class);
    dispatcher = new StandardHandlerDispatcher(personHandlerProvider,
            activityHandlerProvider, appDataHandlerProvider);
  }

  @Test
  public void testGetHandler() {
    PersonHandler handler = mock(PersonHandler.class);
    when(personHandlerProvider.get()).thenReturn(handler);

    assertSame(handler, dispatcher.getHandler(DataServiceServlet.PEOPLE_ROUTE));
  }

  @Test
  public void testGetHandler_serviceDoesntExist() {
    assertNull(dispatcher.getHandler("makebelieve"));
  }

  @Test
  public void testAddHandler() {
    DataRequestHandler mockHandler = mock(DataRequestHandler.class);
    @SuppressWarnings("unchecked")
    Provider<DataRequestHandler> mockProvider = mock(Provider.class);
    dispatcher.addHandler("mock", mockProvider);

    when(mockProvider.get()).thenReturn(mockHandler);

    assertSame(mockHandler, dispatcher.getHandler("mock"));
  }
}
